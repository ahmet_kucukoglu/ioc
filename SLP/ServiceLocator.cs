﻿namespace SLP
{
    using System;
    using System.Collections.Generic;

    public class ServiceLocator
    {
        private static readonly Dictionary<Type, object> services = new Dictionary<Type, object>();

        public static void Register<T>(T service)
        {
            services.Add(typeof(T), service);
        }

        public static T Resolve<T>()
        {
            object service;
            var hasService = services.TryGetValue(typeof(T), out service);

            if (hasService)
            {
                return (T)service;
            }

            return default(T);
        }
    }
}
