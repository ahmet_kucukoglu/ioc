﻿namespace DIP
{
    using System.Collections.Generic;

    public class LogsDispatcher : ILogDispatcher
    {
        private readonly IList<ILog> _logs;

        public LogsDispatcher(IList<ILog> logs)
        {
            _logs = logs;
        }

        public void GoAhead()
        {
            foreach (var _log in _logs)
            {
                try
                {
                    _log.Create();
                    break;
                }
                catch { }
            }
        }
    }
}
