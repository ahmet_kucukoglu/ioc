﻿namespace DIP
{
    using Ninject;

    public class OrderService : IOrderService
    {
        [Inject]
        public ILogDispatcher LogDispatcher { get; set; }
        
        public void CreateOrder()
        {
            //LOGIC

            LogDispatcher.GoAhead();
        }
    }
}
