﻿namespace DIP
{
    public interface ILogDispatcher
    {
        void GoAhead();
    }
}
