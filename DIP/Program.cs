﻿namespace DIP
{
    using Ninject;
    using System;

    class Program
    {
        static void Main(string[] args)
        {
            var kernel = new StandardKernel();
            kernel.Bind<IOrderService>().To<OrderService>();
            //kernel.Bind<ILogDispatcher>().To<LogDispatcher>();
            kernel.Bind<ILogDispatcher>().To<LogsDispatcher>();
            kernel.Bind<ILog>().To<FileLog>().Named("File");
            kernel.Bind<ILog>().To<DbLog>().Named("Db");

            var orderService = kernel.Get<IOrderService>();
            orderService.CreateOrder();

            Console.ReadLine();
        }
    }
}
