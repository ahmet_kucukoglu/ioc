﻿namespace NinjectUI.Models
{
    using Ninject;

    public class LogDispatcher : ILogDispatcher
    {
        private readonly ILog _dbLog;
        private readonly ILog _fileLog;

        public LogDispatcher([Named("Db")]ILog dbLog, [Named("File")]ILog fileLog)
        {
            _dbLog = dbLog;
            _fileLog = fileLog;
        }

        public string GoAhead()
        {
            var result = $"{_fileLog.Create()} {_dbLog.Create()}";

            return result;
        }
    }
}
