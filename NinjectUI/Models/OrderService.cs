﻿namespace NinjectUI.Models
{
    using Ninject;

    public class OrderService : IOrderService
    {
        [Inject]
        public ILogDispatcher LogDispatcher { get; set; }
        
        public string CreateOrder()
        {
            //LOGIC

            return LogDispatcher.GoAhead();
        }
    }
}
