﻿namespace NinjectUI.Models
{
    public interface ILogDispatcher
    {
        string GoAhead();
    }
}
