﻿namespace NinjectUI.Controllers
{
    using Models;
    using Ninject;
    using System.Web.Mvc;

    public class HomeController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly ILog _iLog;

        public HomeController(IOrderService orderService, [Named("File")]ILog iLog)
        {
            _orderService = orderService;
            _iLog = iLog;
        }

        public ActionResult Index()
        {
            var content = _orderService.CreateOrder();

            return Content(content);
        }
    }
}