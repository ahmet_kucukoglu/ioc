﻿[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectUI.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NinjectUI.App_Start.NinjectWebCommon), "Stop")]

namespace NinjectUI.App_Start
{
    using System;
    using System.Web;
    using Ninject;
    using Ninject.Web.Common;
    using Models;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IOrderService>().To<OrderService>();
            kernel.Bind<ILogDispatcher>().To<LogDispatcher>();

            //Default Transient Scope
            //kernel.Bind<ILog>().To<FileLog>().InTransientScope().Named("File");
            //kernel.Bind<ILog>().To<DbLog>().InTransientScope().Named("Db");

            //Bir kere instance oluşturulur
            //kernel.Bind<ILog>().To<FileLog>().InSingletonScope().Named("File");
            //kernel.Bind<ILog>().To<DbLog>().InSingletonScope().Named("Db");

            //Her bir thread için bir kere instance oluşturulur.
            //kernel.Bind<ILog>().To<FileLog>().InThreadScope().Named("File");
            //kernel.Bind<ILog>().To<DbLog>().InThreadScope().Named("Db");

            //Her istekte instance oluşturulur. (Per Request)
            kernel.Bind<ILog>().To<FileLog>().InRequestScope().Named("File");
            kernel.Bind<ILog>().To<DbLog>().InRequestScope().Named("Db");
        }
    }
}
